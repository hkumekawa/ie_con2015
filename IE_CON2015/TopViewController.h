//
//  TopViewController.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/19.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTodayEnergy;
@property (weak, nonatomic) IBOutlet UILabel *lblTodaySaveAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthSumEnergy;
@property (weak, nonatomic) IBOutlet UILabel *lblSumSaveAmount;
@property (weak, nonatomic) IBOutlet UILabel *goalMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblTargetAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblHayaokuri;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblLimitEnergy;
@property (weak, nonatomic) IBOutlet UISegmentedControl *swtSwitch;

@property (weak, nonatomic) IBOutlet UILabel *targetGoods;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *goodsPrice;
@end
