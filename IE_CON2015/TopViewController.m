//
//  TopViewController.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/19.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "TopViewController.h"
#import "MagicalRecord/MagicalRecord.h"
#import "Rakuten.h"
#import "Power.h"
#import "Target.h"


@interface TopViewController ()

@end

@implementation TopViewController {
    int addCnt;
    int timeCnt;
    BOOL outFlg;
    NSDate *nowDate;
    int limitEnergy;
    int totalEnergy;
    BOOL onHardMode;
    NSTimer *setTimer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    addCnt = 0;
    timeCnt = 0;
    outFlg = false;
    nowDate = [NSDate date];
    limitEnergy = [self.lblLimitEnergy.text intValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd 00:00:00";
    
    NSString *formattedDateString = [dateFormatter stringFromDate:nowDate];
    self.lblDate.text = formattedDateString;

    [MagicalRecord setupCoreDataStack];
    
    NSError *error = nil;
    
//    NSArray *all = [Entity MR_findAll];
    
    // データの検索
    NSNumber *count = [Rakuten MR_numberOfEntities];
    NSLog(@"count : %@", count);
    
    NSArray *all = [Rakuten MR_findAll];
    
    
    if(all.count > 0){
        Rakuten *magicalRakuten = all[0];
        
        _targetGoods.text = magicalRakuten.itemName;
        NSURL *imageUrl = [NSURL URLWithString:magicalRakuten.itemImage];
        _goodsImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
        
        NSLog(@"magicalAll : %@", magicalRakuten.itemName);
        NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    }
    else{
        _targetGoods.text = @"まだ設定されていません";
        NSURL *imageUrl = [NSURL URLWithString:@"http://placehold.jp/375x285.png"];
        _goodsImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
        
    }
    
    NSArray *targetAll = [Target MR_findAll];
    
    Target *magicalTarget = nil;
    
    if(targetAll.count > 0){
        magicalTarget = targetAll[0];
        _goodsPrice.text = [NSString stringWithFormat:@"%@",magicalTarget.price];
    }
    else
    {
        _goodsPrice.text = @"0";
    }
    
    
    // 電気機器をつける
    [self runSetPowerOnAll];
    
}

-(void) viewWillAppear:(BOOL)animated{
    // データの検索
    NSNumber *count = [Rakuten MR_numberOfEntities];
    NSLog(@"count : %@", count);
    
    NSArray *all = [Rakuten MR_findAll];
    
    if(all.count > 0){
        Rakuten *magicalRakuten = all[0];
        
        _targetGoods.text = magicalRakuten.itemName;
        NSURL *imageUrl = [NSURL URLWithString:magicalRakuten.itemImage];
        _goodsImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
        
        NSLog(@"magicalAll : %@", magicalRakuten.itemName);
        NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    }
    else{
        _targetGoods.text = @"まだ設定されていません";
        NSURL *imageUrl = [NSURL URLWithString:@"http://placehold.jp/375x285.png"];
        _goodsImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
    }
    
    NSArray *targetAll = [Target MR_findAll];
    
    Target *magicalTarget = nil;
    
    if(targetAll.count > 0){
        magicalTarget = targetAll[0];
        _goodsPrice.text = [NSString stringWithFormat:@"%@",magicalTarget.price];
    }
    else
    {
        _goodsPrice.text = @"0";
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// 設定ボタンがタップされた時
- (IBAction)onTappedSettingBtn:(id)sender {
}

// チュートリアルボタンがタップされた時
- (IBAction)onTappedTutorialBtn:(id)sender {
    // 早送りラベル表示
    self.lblHayaokuri.hidden = false;
    // 各記録値をリセットする
    [self resetValues];
    
    // 早送りラベルを表示する
    self.lblHayaokuri.hidden = NO;
    
}


// 各値をリセットする
- (void) resetValues {
    // 本日の電気使用量
    self.lblTodayEnergy.text = @"0";
    // 本日の節約金額
    self.lblTodaySaveAmount.text = @"0";
    // 累計電気使用量
    self.lblMonthSumEnergy.text = @"0";
    // 累計節約金額
    self.lblSumSaveAmount.text = [NSString stringWithFormat:@"%@%@",@"0",@"円"];
    
    setTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                target:self
                                              selector:@selector(updateLabels:)
                                              userInfo:nil
                                               repeats:YES];
}

// 1秒ごとに呼び出される
- (void) updateLabels:(NSTimer*)timer {
    
    self.lblDate.text = [self calcDate];
    int addValue = 2;
    if (outFlg) {
        addValue = 5;
    }
    totalEnergy += addValue;
    if (addCnt == 23) {
        self.lblTodayEnergy.text = @"0";
        addCnt = 0;
        outFlg = !outFlg;
    } else {
        // 本日の電気使用量
        self.lblTodayEnergy.text = [NSString stringWithFormat:@"%d",[self.lblTodayEnergy.text intValue] + addValue];
        addCnt += 1;
    }
    // 本日の節約金額
    self.lblTodaySaveAmount.text = @"0";
    // 累計電気使用量
    self.lblMonthSumEnergy.text = [NSString stringWithFormat:@"%d",totalEnergy];
    // 累計節約金額
    self.lblSumSaveAmount.text = [NSString stringWithFormat:@"%@%@",@"0",@"円"];
 
    if (onHardMode) {
        // ハードモードがONだったとき
        if ([self.lblTodayEnergy.text intValue] > [self.lblLimitEnergy.text intValue]) {
            [setTimer invalidate];
            Power *power = [[Power alloc] init];
            [power setPowerOff];
            // 本日の上限よりも使用量が多くなった時
            UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"WARNING"
                                                                              message:@"本日の使用量が上限を達した為、全ての電気機器をあなたの望んでいない状態に変更しました。"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
            
            [alertCon addAction:[UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
//                                                           [self runSetPowerOff];
                                                       }]];
            [self presentViewController:alertCon animated:YES completion:nil];
        }
    }
}

// 日付を計算する
- (NSString*)calcDate {
    NSDate *date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd HH:mm:ss";
    date = [dateFormatter dateFromString:self.lblDate.text];
    date = [NSDate dateWithTimeInterval:1*60*60 sinceDate:date];
    timeCnt++;
    
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
}


- (IBAction)onChangedSwitch:(id)sender {
    UISegmentedControl *seg = (UISegmentedControl*)sender;
    switch (seg.selectedSegmentIndex) {
        case 0:
            // ノーマルモードが選択された時
            onHardMode = FALSE;
        case 1:
            // ハードモードが選択された時
            onHardMode = TRUE;
        break;
    }
}

// アラートボタンのOKがタップされた場合の処理
- (void) runSetPowerOff {
    Power *power = [[Power alloc] init];
    [power setPowerOff];
}
// アラートボタンのOKがタップされた場合の処理
- (void) runSetPowerOnAll {
    Power *power = [[Power alloc] init];
    [power setPowerOnAll];
}

@end
