//
//  GoodsDetailController.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/17.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "GoodsDetailController.h"
#import "MagicalRecord/MagicalRecord.h"
#import "Rakuten.h"

@implementation GoodsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_itemDic);
    
    NSArray *imageURLs = [_itemDic objectForKey:@"mediumImageUrls"];
    NSDictionary *imageURLDic = imageURLs[0];
    NSURL *imageUrl = [NSURL URLWithString:[imageURLDic objectForKey:@"imageUrl"]];
    
    _goodsIMage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
    _goodsName.text = [_itemDic objectForKey:@"itemName"];
    _goodsCatch.text = [_itemDic objectForKey:@"itemCaption"];
    _goodsPrice.text = [NSString stringWithFormat:@"%@",[_itemDic objectForKey:@"itemPrice"]];
    
}


- (IBAction)btnSetGoods:(id)sender {

    [MagicalRecord setupCoreDataStack];
    
    NSNumber *count = [Rakuten MR_numberOfEntities];
    NSLog(@"count : %@", count);
    
    Rakuten *magicalRakuten = nil;
    
    magicalRakuten = [Rakuten MR_createEntity];
    
    magicalRakuten.id = [NSNumber numberWithInt:1];
    magicalRakuten.itemName = [_itemDic objectForKey:@"itemName"];
    NSArray *imageURLs = [_itemDic objectForKey:@"mediumImageUrls"];
    NSDictionary *imageURLDic = imageURLs[0];
    magicalRakuten.itemImage = [imageURLDic objectForKey:@"imageUrl"];
    magicalRakuten.itemCode = [_itemDic objectForKey:@"itemCode"];
    magicalRakuten.itemPrice = [_itemDic objectForKey:@"itemPrice"];
    NSLog(@"%@",[_itemDic objectForKey:@"itemPrice"]);

     [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    NSLog(@"OK");
    NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    
    
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"設定完了"
                                                                      message:@"アイテムを設定しました。"
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCon addAction:[UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
                                                   //                                                           [self runSetPowerOff];
                                               }]];
    [self presentViewController:alertCon animated:YES completion:nil];
    
    
}
@end
