//
//  ViewController.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/15.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "ViewController.h"
#import "GoodsListViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSearch:(UIButton *)sender {
        
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:@"toGoodsList"]){
        GoodsListViewController *next = [segue destinationViewController];
        next.goodsName = [NSString stringWithFormat:@"%@",_goodsName.text];
    }
}

@end
