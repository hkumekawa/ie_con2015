//
//  GoodsListViewController.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/16.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "GoodsDetailController.h"

@interface GoodsListViewController : UITableViewController

@property (weak,nonatomic) NSString *goodsName;
@property NSArray *itemsArray;

@end
