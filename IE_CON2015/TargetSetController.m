//
//  TargetSetController.m
//  IE_CON2015
//
//  Created by 粂川洋 on 2015/09/30.
//  Copyright © 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "TargetSetController.h"
#import "MagicalRecord/MagicalRecord.h"
#import "Target.h"

@interface TargetSetController ()

@end

@implementation TargetSetController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [MagicalRecord setupCoreDataStack];
    
    NSNumber *count = [Target MR_numberOfEntities];
    
    NSArray *all = [Target MR_findAll];
    
    Target *magicalTarget = nil;
    
    if(all.count > 0){
        magicalTarget = all[0];
        
        _price.text = [NSString stringWithFormat:@"%@",magicalTarget.price];
        
        NSLog(@"magicalAll : %@", magicalTarget.price);
        NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    }
    else
    {
        _price.text = @"0";
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)setTargetPrice:(UIButton *)sender {
    
    [MagicalRecord setupCoreDataStack];
    
    NSNumber *count = [Target MR_numberOfEntities];
    
    NSArray *all = [Target MR_findAll];
    
    Target *magicalTarget = nil;
    
    if(all.count > 0){
        magicalTarget = all[0];
        
        NSLog(@"magicalAll : %@", magicalTarget.price);
        NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    }
    else
    {
        magicalTarget = [Target MR_createEntity];
    }
    
    magicalTarget.id = [NSNumber numberWithInt:1];
    magicalTarget.price = [NSNumber numberWithInt:[_price.text intValue]];
    NSLog(@"%@",[NSNumber numberWithInt:[_price.text intValue]]);
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    NSLog(@"OK");
    NSLog(@"%@",[NSPersistentStore MR_urlForStoreName:[MagicalRecord defaultStoreName]]);
    
    
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"設定完了"
                                                                      message:@"金額を設定しました。"
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCon addAction:[UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
                                                   //                                                           [self runSetPowerOff];
                                               }]];
    [self presentViewController:alertCon animated:YES completion:nil];
    
}
@end
