//
//  GoodsListViewController.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/16.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "GoodsListViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface GoodsListViewController ()<UITableViewDelegate, UITableViewDataSource>



@end

@implementation GoodsListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _itemsArray = [NSArray array];
    
    NSString *baseURL = @"https://app.rakuten.co.jp/services/api/IchibaItem/Search/20140222?format=json&applicationId=1044873967106007873&keyword=";
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL,[_goodsName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@",requestURL);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        _itemsArray = [responseObject objectForKey:@"Items"];
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger dataCount;
    dataCount = _itemsArray.count;
    
    return dataCount;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GoodsCell";
    // 再利用できるセルがあれば再利用する
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *itemDic = [NSDictionary dictionary];
    itemDic = [_itemsArray[indexPath.row] objectForKey:@"Item"];
    cell.textLabel.text = [itemDic objectForKey:@"itemName"];
    
    NSArray *imageURLs = [itemDic objectForKey:@"smallImageUrls"];
    NSDictionary *imageURLDic = imageURLs[0];
    NSURL *imageUrl = [NSURL URLWithString:[imageURLDic objectForKey:@"imageUrl"]];
    
    cell.imageView.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:imageUrl]];
    
    cell.clipsToBounds = YES;//frameサイズ外を描画しない
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // 選択状態の解除をします。
    

    NSDictionary *itemDic = [_itemsArray[indexPath.row] objectForKey:@"Item"];
    
    // ストーリーボードを指定する
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    // 遷移先のViewControllerをStoryBoardをもとに作成
    GoodsDetailController *next = [storyboard instantiateViewControllerWithIdentifier:@"boardGoodsDetail"];
    
    next.itemDic = itemDic;

    [self.navigationController pushViewController:next animated:YES];
}

@end
