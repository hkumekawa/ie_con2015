//
//  ViewController.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/15.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Rakuten.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *goodsName;
- (IBAction)btnSearch:(UIButton *)sender;

@end

