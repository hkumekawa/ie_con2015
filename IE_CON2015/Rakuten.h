//
//  Entity.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/19.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Rakuten : NSManagedObject

@property (nonatomic, retain) NSString * itemCode;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemImage;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * itemPrice;

@end
