//
//  main.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/15.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
