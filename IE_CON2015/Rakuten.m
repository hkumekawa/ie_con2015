//
//  Entity.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/19.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "Rakuten.h"


@implementation Rakuten

@dynamic itemCode;
@dynamic itemName;
@dynamic itemImage;
@dynamic id;
@dynamic itemPrice;

@end
