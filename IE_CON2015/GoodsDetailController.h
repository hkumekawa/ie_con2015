//
//  GoodsDetailController.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/17.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsDetailController : UIViewController

@property (weak,nonatomic) NSDictionary *itemDic;

@property (weak, nonatomic) IBOutlet UIImageView *goodsIMage;
@property (weak, nonatomic) IBOutlet UILabel *goodsName;
@property (weak, nonatomic) IBOutlet UITextView *goodsCatch;
@property (weak, nonatomic) IBOutlet UILabel *goodsPrice;
- (IBAction)btnSetGoods:(id)sender;
@end
