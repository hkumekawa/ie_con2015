//
//  TargetSetController.h
//  IE_CON2015
//
//  Created by 粂川洋 on 2015/09/30.
//  Copyright © 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetSetController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *price;
- (IBAction)setTargetPrice:(UIButton *)sender;

@end
