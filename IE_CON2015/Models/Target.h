//
//  Target.h
//  IE_CON2015
//
//  Created by 粂川洋 on 2015/10/01.
//  Copyright © 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface Target : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * price;


@end
