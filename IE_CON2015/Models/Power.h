//
//  Power.h
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/16.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Power : NSObject

// 今日の消費電力を取得
-(NSString *)today;

// 今日の電気料金を取得
-(int *)todayCharges;

// 今月の消費電力を取得
-(NSString *)thisMonth;

// 今月の電気料金を取得
-(int *)thisMonthCharges;

// 電気をOFFする
- (void)setPowerOff;
// 全ての電気機器のスイッチをONにする
- (void) setPowerOnAll;
@end
