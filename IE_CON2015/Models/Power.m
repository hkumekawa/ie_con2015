//
//  Power.m
//  IE_CON2015
//
//  Created by 粂川 洋 on 2015/07/16.
//  Copyright (c) 2015年 Hiroshi Kumekawa. All rights reserved.
//

#import "Power.h"
#import "AFHTTPRequestOperationManager.h"

@implementation Power

- (void)setPowerOff {
    // 照明OFF
    NSString *baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.ledLight_1_1&type=codeset&key=80&value=31";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    // 電動窓OPEN
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_3_1&type=set&key=operationStatus&value=ON";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    // 電動シャッターOPEN
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_2_1&type=set&key=operationStatus&value=OFF";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    // 電動シャッターOPENになったら、エアコンOFF
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.aircon_1_1&type=set&key=operationStatus&value=OFF";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    // 電気鍵開ける
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_1_1&type=set&key=operationStatus&value=OFF";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

// 全ての電気機器のスイッチをONにする
- (void) setPowerOnAll {
    
    //照明ON
    NSString *baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.ledLight_1_1&type=codeset&key=80&value=30";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    // 電動窓CLOSE
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_3_1&type=set&key=operationStatus&value=OFF";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    // 電動シャッターCLOSE
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_2_1&type=set&key=operationStatus&value=ON";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    // エアコンON
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.aircon_1_1&type=set&key=operationStatus&value=ON";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    // 電気鍵閉める
    baseURL = @"http://192.168.100.59:1024/smart/rest/request?deviceid=lite.switch_1_1&type=set&key=operationStatus&value=ON";
    manager = [AFHTTPRequestOperationManager manager];
    [manager GET:baseURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
